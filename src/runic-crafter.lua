local AE2 = peripheral.wrap("ae2_1")
local ME = peripheral.wrap("meBridge_1")
local INPUT_BARREL = peripheral.wrap("minecraft:barrel_0")
local CRAFTING_BARREL = peripheral.wrap("minecraft:barrel_1")
local MOTOR = peripheral.wrap("electric_motor_0")
local VACUUM = peripheral.wrap("minecraft:barrel_3")

-- List of all possible craftable items.
local OUTPUT_NAMES = {
    "botania:rune_air",
    "botania:rune_autumn",
    "botania:rune_earth",
    "botania:rune_envy",
    "botania:rune_fire",
    "botania:rune_gluttony",
    "botania:rune_greed",
    "botania:rune_lust",
    "botania:rune_mana",
    "botania:rune_pride",
    "botania:rune_sloth",
    "botania:rune_spring",
    "botania:rune_summer",
    "botania:rune_water",
    "botania:rune_winter",
    "botania:rune_wrath",
    "extrabotany:aerostone",
    "extrabotany:aquastone",
    "extrabotany:deathring",
    "extrabotany:earthstone",
    "extrabotany:firstfractal",
    "extrabotany:froststar",
    "extrabotany:gildedpotato",
    "extrabotany:ignisstone",
    "extrabotany:moonpendant",
    "extrabotany:photonium",
    "extrabotany:potatochips",
    "extrabotany:powerglove",
    "extrabotany:shadowium",
    "extrabotany:sunring",
    "extrabotany:thecommunity",
    "minecraft:player_head",
    "mythicbotany:alfheim_rune",
    "mythicbotany:asgard_rune",
    "mythicbotany:helheim_rune",
    "mythicbotany:joetunheim_rune",
    "mythicbotany:midgard_rune",
    "mythicbotany:muspelheim_rune",
    "mythicbotany:nidavellir_rune",
    "mythicbotany:niflheim_rune",
    "mythicbotany:vanaheim_rune",
}

-- List of items to skip because there are multiple configured auto-crafting recipes.
local skipped = {}

while true do
    for _, outputName in ipairs(OUTPUT_NAMES) do
        repeat
            -- Skip items previously marked as having multiple recipes.
            for _, toSkip in ipairs(skipped) do
                if outputName == toSkip then
                    break
                end
            end

            local output = { name = outputName }
            local recipes = AE2.getPatterns(output)

            if #recipes ~= 0 then
                if #recipes > 1 then
                    print("Item '" .. outputName .. "' has more than one configured recipe!")
                    print("It will be skipped going forward. Restart the program to re-instate it.")

                    skipped.insert(outputName)
                    break
                end

                -- Only craft when requested.
                if ME.isItemCrafting(output) then
                    print("Crafting " .. outputName)

                    local availInputs = INPUT_BARREL.list()

                    for _, input in ipairs(recipes[1].inputs) do
                        repeat
                            for slot, availInput in pairs(availInputs) do
                                if availInput.name == input.name and availInput.count >= input.count then
                                    print(" > Depositing " .. input.count .. "x " .. input.name .. "...")
                                    INPUT_BARREL.pushItems(peripheral.getName(CRAFTING_BARREL), slot, input.count)
                                    break
                                end
                            end
                        until true
                    end

                    print(" > Waiting for mana...")

                    -- Wait to finalise crafting.
                    repeat
                        --- @diagnostic disable-next-line: undefined-field :: exists in CraftOS 1.8
                        os.pullEvent("redstone")
                    until redstone.getAnalogInput("left") == 2

                    print(" > Depositing Livingrock...")
                    ME.exportItemToPeripheral({ name = "botania:livingrock" }, peripheral.getName(CRAFTING_BARREL))

                    print(" > Smackin' it!")
                    MOTOR.setSpeed(256)

                    local vacuumItemPresent = false

                    repeat
                        local vacuumItem = VACUUM.getItemDetail(1)

                        if vacuumItem then
                            vacuumItemPresent = vacuumItem.name == outputName
                        end
                    until vacuumItemPresent

                    MOTOR.stop()

                    print(" > Returning output to ME system...")
                    ME.importItemFromPeripheral(output, peripheral.getName(VACUUM))

                    print(" > ... done!")
                end
            end
        until true
    end
end
